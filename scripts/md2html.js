'use strict';

const readline = require('readline');
const highlighter = require('highlight.js');
const marked = require('marked');


async function getInput() {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
  });
  return new Promise(resolve => {
    const stdinLines = [];
    rl.on('line', line => stdinLines.push(line));
    rl.on('close', () => resolve(stdinLines.join('\n')));
  });
}

function md2Html(src) {
  marked.setOptions({
    breaks: true,
    highlight: code => highlighter.highlightAuto(code).value,
    smartLists: true,
  });
  return marked(src);
}

async function main() {
  const input = await getInput();
  console.log(md2Html(input));
}

main();