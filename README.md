# 4s

Super simple static site

Content format:

```md
---
datetime: 2018-12-10T16:16:55Z
description: Getting started with 4S
publish: true
---

# Title

Regular Markdown content.

# Further heading 1 is not counted as title
```

## Dependencies

- Bash
- `envsubst` (on MacOS you need to install `gettext`)
- Make
- Node.js