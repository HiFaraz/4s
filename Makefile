# Output variables
OUT_DIR := public

all: help

help:
	@echo "Project Makefile."
	@echo
	@echo "Usage:"
	@echo "  make, make help  Show this help message."
	@echo "  make clean       Remove all compiled content."
	@echo "  make content     Compile all content once."
	@echo "  make index       Compile content index."
	@echo "  make watch       Run all content in watch mode."
	@echo

.PHONY : clean # prevent conflict if there is an actual file called clean
clean:
	@echo Remove all compiled content.
	@rm -rf $(OUT_DIR)

content: clean
	@make -f builder/Makefile

index:
	@make -f indexer/Makefile --always-make

watch:
	@$(MAKE) -s watch-inner

watch-inner:
	@while true; do \
		$(MAKE) --no-print-directory content; \
		inotifywait -qqre close_write .; \
	done
